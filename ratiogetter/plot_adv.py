import matplotlib.pyplot as plt 
import matplotlib.dates as mdates
import sqlite3
import datetime
import time
import numpy as np

conn = sqlite3.connect('ratios.db')
c = conn.cursor()
sets = {
	'Amonkhet':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Amonkhet-Booster-Box',
    'Hour of Devastation':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Hour-of-Devastation-Booster-Box',
	'Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ixalan-Booster-Box',
	'Unstable':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Unstable-Booster-Box',
	'Rivals of Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Rivals-of-Ixalan-Booster-Box',
	'Dominaria':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Dominaria-Booster-Box',
	'Battlebond':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Battlebond-Booster-Box',
	'Core Set 2019':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2019-Booster-Box',
	'Guilds of Ravnica':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Guilds-of-Ravnica-Booster-Box',
	'Ravnica Allegiance':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ravnica-Allegiance-Booster-Box',
	'War of the Spark':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/War-of-the-Spark-Booster-Box',
	'Modern Horizons':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Modern-Horizons-Booster-Box',
	'Core Set 2020':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2020-Booster-Box',
	'Throne of Eldraine':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Throne-of-Eldraine-Booster-Box',
	'Masters 25':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Masters-25-Booster-Box',
	'Iconic Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Iconic-Masters-Booster-Box',
	'Ultimate Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ultimate-Masters-Booster-Box'
}

plt.figure(figsize=(18,8))
plt.xlabel('Date', fontsize = 'large')
plt.ylabel('Availability/Price', fontsize = 'large')
plt.grid()

# Gets ratios & timestamp from DB and puts them into plottable array + vector for polyfit

def getratios(magicset):
	c.execute("SELECT ts, price FROM ratios WHERE magicset = '" + magicset + "'")
	dates = []
	ratios = []
	for row in c.fetchall():
		dates.append(datetime.datetime.fromtimestamp(row[0]))
		ratios.append(row[1])
	vectors = np.stack((mdates.date2num(dates), ratios), axis=-1)
	return(vectors)

def definepoly(vectors):
	x = vectors[:,0]
	y = vectors[:,1]
	z = np.polyfit(x, y, 20)
	polynomial = np.poly1d(z)
	return(polynomial, x, y)

def draw(polynomial, x, y, magicset):
#	x_new = np.linspace(x[0], x[-1])
	y_new = polynomial(x)
#	plt.plot(x, y, '-', label = magicset, linewidth = 3)
	plt.plot(mdates.num2date(x), y_new, '-', label = magicset, linewidth = 3)
	plt.legend(fontsize = 'large')

def plotter(key):
	vectors = getratios(key)
	polynomial, x, y = definepoly(vectors)
	draw(polynomial, x, y, key)

for key in sets:
	plotter(key)

conn.close()
plt.show()



