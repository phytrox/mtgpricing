from flask import Flask, render_template
import sqlite3
import datetime
import time
import numpy as np
#import matplotlib.pyplot as plt 
import matplotlib.dates as mdates
app = Flask(__name__)
sets = {
	'Amonkhet':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Amonkhet-Booster-Box',
    'Hour of Devastation':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Hour-of-Devastation-Booster-Box',
	'Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ixalan-Booster-Box',
	'Unstable':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Unstable-Booster-Box',
	'Rivals of Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Rivals-of-Ixalan-Booster-Box',
	'Dominaria':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Dominaria-Booster-Box',
	'Battlebond':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Battlebond-Booster-Box',
	'Core Set 2019':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2019-Booster-Box',
	'Guilds of Ravnica':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Guilds-of-Ravnica-Booster-Box',
	'Ravnica Allegiance':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ravnica-Allegiance-Booster-Box',
	'War of the Spark':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/War-of-the-Spark-Booster-Box',
	'Modern Horizons':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Modern-Horizons-Booster-Box',
	'Core Set 2020':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2020-Booster-Box',
	'Throne of Eldraine':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Throne-of-Eldraine-Booster-Box',
	'Masters 25':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Masters-25-Booster-Box',
	'Iconic Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Iconic-Masters-Booster-Box',
	'Ultimate Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ultimate-Masters-Booster-Box'
}

def getprices(magicset):
	conn = sqlite3.connect('ratios.db')
	c = conn.cursor()
	c.execute("SELECT ts, price FROM ratios WHERE magicset = '" + magicset + "'")
	dates = []
	prices = []
	for row in c.fetchall():
		dates.append(datetime.datetime.fromtimestamp(row[0]))
		prices.append(row[1])
	vectors = np.stack((mdates.date2num(dates), prices), axis=-1)
	x = vectors[:,0]
	y = vectors[:,1]
	conn.close()
	return(x, y)
	
@app.route("/")
@app.route("/home")
def load():
	magicset = 'Hour of Devastation'
	#TO DO 
	#define object with all the data
	#for key in sets
		#x, y = getprices(magicset)
		#append objects to main object
	#pass the complex object with all the data to render template
	x, y = getprices(magicset)
	x = x.tolist()
	y = y.tolist()
	return render_template('home.html', x = x, y = y, magicset = magicset)
	

	
if __name__ == '__main__':
	app.run(debug=True)
