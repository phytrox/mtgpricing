
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import sqlite3
import random
from datetime import datetime

chrome_path = '/usr/lib/chromium-browser/chromedriver'
options = Options()
options.add_experimental_option("detach", True)
options.add_argument("--window-position=0,0")
options.add_argument("--headless")

driver = webdriver.Chrome(executable_path = chrome_path, chrome_options=options)
conn = sqlite3.connect('/home/pi/Documents/mtgpricing/ratiogetter/ratios.db')
c = conn.cursor()

sets = {
        'Amonkhet':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Amonkhet-Booster-Box',
        'Hour of Devastation':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Hour-of-Devastation-Booster-Box',
	'Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ixalan-Booster-Box',
	'Unstable':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Unstable-Booster-Box',
	'Rivals of Ixalan':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Rivals-of-Ixalan-Booster-Box',
	'Dominaria':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Dominaria-Booster-Box',
	'Battlebond':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Battlebond-Booster-Box',
	'Core Set 2019':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2019-Booster-Box',
	'Guilds of Ravnica':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Guilds-of-Ravnica-Booster-Box',
	'Ravnica Allegiance':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ravnica-Allegiance-Booster-Box',
	'War of the Spark':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/War-of-the-Spark-Booster-Box',
	'Modern Horizons':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Modern-Horizons-Booster-Box',
	'Core Set 2020':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Core-2020-Booster-Box',
	'Throne of Eldraine':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Throne-of-Eldraine-Booster-Box',
	'Masters 25':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Masters-25-Booster-Box',
	'Iconic Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Iconic-Masters-Booster-Box',
	'Ultimate Masters':'https://www.cardmarket.com/en/Magic/Products/Booster-Boxes/Ultimate-Masters-Booster-Box'
}

def store(timestamp, magicset, price, count, ratio, volume, desirability):
	c.execute("INSERT INTO ratios VALUES (" + str(timestamp) + ", '" + magicset + "', '" + str(price) + "', '" + str(count) + "', '" + str(ratio) +"',  '" + str(volume) +"',  '" + str(desirability) +"')")
 
def collectratio(magicset, magiclink):
    driver.get(magiclink)
    ts = time.time()
    #Getting number of "Available items"
    count = WebDriverWait(driver, 100).until(
    	EC.presence_of_element_located((By.XPATH, '//*[@id="tabContent-info"]/div/div[1]/div/div[2]/dl/dd[1]'))
    	)
    #Getting "Price Trend"
    price = WebDriverWait(driver, 100).until(
    	EC.presence_of_element_located((By.XPATH, '//*[@id="tabContent-info"]/div/div[1]/div/div[2]/dl/dd[3]/span'))
    	)
    count = int(count.text)
    price = str(price.text)
    price = price[:-2]
    price = float(price.replace(',', '.'))
    ratio = round(float((count)/(price)), 3)
    volume = round(price*count, 0)
    desirability = round((count*count)/price)
    store(ts, key, price, count, ratio, volume, desirability)
    print (datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d') + ' ' + magicset + ': Ratio = ' + str(ratio)+ ', Value on market = ' + str(desirability))
    time.sleep(random.randint(3,8))

try:
    for key, value in sets.items():
        collectratio(key, value)
finally:
    driver.quit()
    
conn.commit()
conn.close()
print('Collection successful!')
